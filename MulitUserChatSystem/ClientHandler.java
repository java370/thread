import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;

public class ClientHandler implements Runnable {
    // for broadcasting messages to each user
    // we need arraylist of client objects
    public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private String clinetUserName;

    public ClientHandler(Socket socket) {
        // Constructor for initializing all the data of ClientHandler
        try {
            this.socket = socket;
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            // get the username from client application
            // when a client joins the chat
            this.clinetUserName = bufferedReader.readLine();
            // assign all data to particular client object
            clientHandlers.add(this);
            broadcastMessage("# SERVER: " + this.clinetUserName + " joinded this chat!");
        } catch (IOException e) {
            closeAll(socket, bufferedReader, bufferedWriter);
        }
    }

    /**
     * In this run() method a thread is running
     * this thred is responsible for sending clients messages
     * to all other group users.
     */

    @Override
    public void run() {
        String messageFromClient = null;
        // infinite loop
        while (socket.isConnected()) {
            try {
                messageFromClient = bufferedReader.readLine();
                broadcastMessage(messageFromClient);
            } catch (Exception e) {
                closeAll(socket, bufferedReader, bufferedWriter);
                break;
            }
        }
    }

    /**
     * This message is being sending to all the members of group
     * here we need client username and arraylist to tracking
     * @param messageToSendAll
     */
    public void broadcastMessage(String messageToSendAll) {
        for (ClientHandler clientHandler : clientHandlers) {
            try {
                if (!clientHandler.clinetUserName.equals(this.clinetUserName)) {
                    clientHandler.bufferedWriter.write(messageToSendAll);
                    clientHandler.bufferedWriter.newLine();
                    clientHandler.bufferedWriter.flush();
                }
            } catch (Exception e) {
                closeAll(socket, bufferedReader, bufferedWriter);
            }
        }
    }

    /**
     * If any client leavs the chat or terminates the program
     * then this method works
     */
    public void removeClientHandler() {
        clientHandlers.remove(this);
        broadcastMessage("# SERVER: " + this.clinetUserName + " has left the chat");
    }

    /**
     * This will invoke on termination of client java program
     * @param socket
     * @param bufferedReader
     * @param bufferedWriter
     */
    public void closeAll(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter) {
        removeClientHandler();
        try {
            if (bufferedReader != null) {
                bufferedReader.close();

            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
